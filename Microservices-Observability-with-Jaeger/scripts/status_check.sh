#!/bin/bash

# Define base directory
BASE_DIR="/Users/chappy/chappy/Coding/Projects/free-time-projects/Project/jaeger/jaeger-1/Microservices-Observability-with-Jaeger"

# Check status of Kubernetes deployments
echo "Checking status of Kubernetes deployments..."
kubectl get deployments -A

# Check status of Kubernetes services
echo "Checking status of Kubernetes services..."
kubectl get services -A
