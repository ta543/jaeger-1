#!/bin/bash

# Define directories
CONFIG_DIR="/Users/chappy/chappy/Coding/Projects/free-time-projects/Project/jaeger/jaeger-1/Microservices-Observability-with-Jaeger/config"
BACKUP_DIR="/Users/chappy/chappy/Coding/Projects/free-time-projects/Project/jaeger/jaeger-1/Microservices-Observability-with-Jaeger/backup"
DATE=$(date +%Y%m%d_%H%M%S)

echo "Backing up configuration files..."
mkdir -p \$BACKUP_DIR/\$DATE
cp -a \$CONFIG_DIR/* \$BACKUP_DIR/\$DATE/

echo "Backup completed successfully at \$BACKUP_DIR/\$DATE"
