#!/bin/bash

# Define base directory
BASE_DIR="/Users/chappy/chappy/Coding/Projects/free-time-projects/Project/jaeger/jaeger-1/Microservices-Observability-with-Jaeger"

# Undeploy microservices
echo "Undeploying microservices..."
kubectl delete -f \$BASE_DIR/deployments/overlays/production
kubectl delete -f \$BASE_DIR/deployments/base

# Undeploy monitoring tools
echo "Undeploying Prometheus..."
kubectl delete -f \$BASE_DIR/monitoring/prometheus/k8s

echo "Undeploying Grafana..."
kubectl delete -f \$BASE_DIR/monitoring/grafana/k8s

echo "Cleanup completed successfully."
