#!/bin/bash

# Define base directory
BASE_DIR="/Users/chappy/chappy/Coding/Projects/free-time-projects/Project/jaeger/jaeger-1/Microservices-Observability-with-Jaeger"

# Deploy microservices
echo "Deploying microservices..."
kubectl apply -f \$BASE_DIR/deployments/base
kubectl apply -f \$BASE_DIR/deployments/overlays/production

# Deploy monitoring tools
echo "Deploying Prometheus..."
kubectl apply -f \$BASE_DIR/monitoring/prometheus/k8s

echo "Deploying Grafana..."
kubectl apply -f \$BASE_DIR/monitoring/grafana/k8s

echo "Deployment completed successfully."
