# plugins/custom_filter.rb
module Fluent
  class TextParser
    class CustomFilter < Parser
      Fluent::Plugin.register_parser("custom_filter", self)

      def parse(text)
        # Your custom parsing logic here
        yield time, record
      end
    end
  end
end
