# 📱 AppTracker Microservices Architecture

Welcome to the **AppTracker** repository, a microservices-based application meticulously crafted to manage user interactions and data analytics effectively. Leveraging the power of Jaeger for distributed tracing, this project is your go-to for high observability across services. Dive into our detailed documentation, focusing primarily on `UserService`.

## 🏗️ Architecture Overview

AppTracker is constructed with a suite of microservices, each tailored to a specific functionality of the application. Here's what our repository encompasses:

- **UserService** 🧑‍💻: Takes care of user authentication, registration, and profile management.
- **OrderService** 📦: Manages order processing and tracking.
- **PaymentService** 💳: Oversees payments and financial transactions.

Integration with Jaeger enables sophisticated tracing and monitoring, elevating system observability and debugging.

## 🚀 Getting Started

Follow these instructions to get a copy of the project up and running on your local machine for development and testing purposes.

### 📋 Prerequisites

Before you begin, ensure you have the following installed:
- Docker 🐳
- Docker Compose
- Git

### 🔧 Installing

Here’s a step-by-step guide to setting up your development environment:

1. **Clone the repository**:
   ```bash
   git clone https://gitlab.com/ta543/jaeger-1.git
   cd jaeger-1
   ```

Build and run the services using Docker Compose:

```bash
docker-compose up --build
```

## 🧪 Running the Tests

Execute the following command to run the automated tests for this system:
```bash
docker exec -it user_service python -m unittest
```

## 🚢 Deployment

For deployment instructions and more on how to utilize CI/CD pipelines, refer to the `.gitlab-ci.yml` file. This will guide you through deploying the system on a live environment.

Enjoy building and scaling your application with **AppTracker**! 🚀















